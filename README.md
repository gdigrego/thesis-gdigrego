# thesis-gdigrego

This repository contains Giulia's PhD tehsis. It is empty since she did not do anything during her PhD.
Before sending in the thesis
* Add coverpage

## How to checkout the note

Running
```
git clone ssh://git@gitlab.cern.ch:7999/gdigrego/thesis-gdigrego.git
```
creates a package thesis-gdigrego holding all the tex files etc for the int note.

## How to compile the note

```
make
```

Comment: on lxplus one needs to set first
```
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:$PATH

```
to have the proper textlive version (2016) setup.


## Template generation

The template was generated from atlaslatex-08-01-00 retaining only the minimal configuration.

*  git clone ssh://git@gitlab.cern.ch:7999/atlas-physics-office/atlaslatex.git
*  cd atlaslatex
*  git checkout atlaslatex-08-01-00
*  make newbook [BASENAME=thesis-gdigrego] [TEXLIVE=2016]

The following files anbd folders were copied:
* thesis-gdigrego\*
* Makefile
* latex
* logos
* bib

## Clean up and additional packages





