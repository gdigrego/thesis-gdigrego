\chapter{The Tile Calorimeter design,  calibration methods and the photomultiplier stability}
\label{app:TileCal}
\textit{This appendix briefly describes the Tile Calorimeter structure, together with the calibration methods used to monitor the stability and the performance of each part of the read-out chain. The second part of the appendix is focused on the response stability of the photomultiplier reading out the Tile Calorimeter.}

\section{The Tile Calorimeter layout}
The Tile Calorimeter is the central section ($|\eta| < 1.7$) of the hadronic calorimeter made up of steel plates, as absorbers, and plastic scintillating tiles, as active materials. The calorimeter is subdivided in three regions along the $z$-axis: a central section called Long Barrel, and two external sections called Extended Barrel (EB). The LB covers the region $|\eta| < 1$, while the EB covers two regions of $0.8 < |\eta| < 1.7$. Full azimuthal coverage around the beam axis is achieved with 64 wedge-shaped modules, each covering $\Delta \phi$ = 0.1 radians.\\
As shown in Fig. \ref{fig:TileCal_module}, the tiles are perpendicular to the beam axis and the light produced by the scintillating tiles at particle crossing is collected by wave-length shifting fibres. The fibres deliver the light to the two photomultipliers (PMTs)\footnote{The photomultiplier is a device which converts light pulses into electron current pulses through a photo-electron emission and charge amplification. } located in the outer radius iron structure that also houses the front-end electronics.  The PMT outputs are amplified, shaped and finally digitized by ADCs, and then stored in the front-end pipeline memory.
\begin{figure}[!htb]
 	\centering
 	\includegraphics[width=0.35\textwidth]{figures/chapter_3/TileCal_Module.pdf}
 	\caption{Illustration of the mechanical assembly and optical read-out of a single TileCal modules \cite{TileCalPubPlots}. }
 	\label{fig:TileCal_module}
 \end{figure}

TileCal is subdivided in three separate longitudinal sampling layers, called respectively A, BC (just B in the EB) and D. TileCal cells, shown in Fig. \ref{fig:TileCal_cells}, are defined by grouping the wave-leght shifting fibres from individual tiles on the corresponding PMT. The granularity of each cell is $\Delta \eta \times \Delta \phi = 0.1 \times 0.1$ for the A and BC (or B) innermost layers, while the cells of the D layer have a granularity of $\Delta \eta \times \Delta \phi = 0.2 \times 0.1$. Between the LB and the EB there are the E cells which are only composed of scintillator and exceptionally read out by only one PMT.
\begin{figure}[!htb]
 	\centering
 	\includegraphics[width=0.8\textwidth]{figures/chapter_3/TileCal_Cells_and_Rows2.pdf}
 	\caption{The layout of the TileCal cells, denoted by a letter (A to E) plus an integer number. The A-layer is closest to the beam-line \cite{TileCalPubPlots}. }
 	\label{fig:TileCal_cells}
 \end{figure}

\section{Calibration methods}
To monitor the TileCal response stability a continuous multi-stage calibration procedure is used. Three calibration systems are used to monitor the stability of each channel of the read-out chain and to provide the correct calibration. The reconstructed energy of each channel of TileCal, $E$ [\GeV],  is expressed in term of the ADC raw response, $A$[ADC], as it follows:
\begin{equation}
E[\GeV] = A [\text{ADC}] \cdot C_{\text{ADC} \to \text{pC}} \cdot C_{\text{pC} \to \GeV} \cdot C_{\text{Cesium}} \cdot C_{\text{Laser}}
\end{equation}
The conversion factor from pC to \GeV, $C_{\text{pC} \to \GeV}$, was fixed during the test beam campaigns using particles with known energy that cross the calorimeter. The remaining calibration constants are provided by the three calibration systems, as shown in Fig. \ref{fig:TileCal_calibration_path}:
\begin{itemize}
\item \textit{Cesium calibration system}: this calibration system is based on a movable radioactive sources which is moving through the calorimeter using a hydraulic system. The response of all TileCal cells is equalized with a Cesium system by monitoring the cell and the PMT response to the source emission.  This calibration system provides the $C_{\text{Cesium}}$ constant.
\item \textit{Laser calibration system}: this calibration system consists of a laser source that sends light pulses to all the PMTs through an optical system.  The laser light pulses are similar to those produced by ionizing particles. The systems is used to monitor and to correct the PMT response variations between two Cesium scans.  Laser calibrations are performed twice per week when particles are not colliding in the LHC. The Laser system provides the laser calibration constant $C_{\text{Laser}}$ which is calculated for each channel and with respect to a reference run taken just after the last Cesium scan.
\item \textit{Charge Injection system} (CIS): this system simulates a physical signal in the channels by injecting a known charge into the ADCs and measuring the electronic readout response. The system provides a relationship between the injected analog signal in pC and the electronic response of the read-out channels. A calibration constant $C_{\text{ADC} \to \text{pC}}$ is determined for each TileCal channel. The CIS calibrations are performed during each machine filling between two consequent physics runs of LHC.
\end{itemize}
Additionally, the use of the signal integrators in Minimum Bias (MB) events is another diagnostic tool developed to cross-check the previous calibration systems. The minimum bias integration is based on measuring the detector activity induced by \textit{p-p} collisions at small transferred momentum. The rate of the MB events is proportional to the instantaneous luminosity and these particular events can be used to monitor the calorimeter response variation.
\begin{figure}[!htb]
 	\centering
 	\includegraphics[width=0.8\textwidth]{figures/chapter_3/TileCalCalibrationChain-color.pdf}
 	\caption{The signal paths for each of the three calibration systems used by TileCal. The physics signal is denoted by the solid line, while the path taken by each calibration system is shown with dashed lines \cite{TileCalPubPlots}.}
 	\label{fig:TileCal_calibration_path}
 \end{figure}

The absolute calibration of the TileCal energy scale is obtained using the Cesium system. However, since the Cesium scans need a pause in the \textit{p-p} collisions of at least six hours, this calibration procedure cannot be performed very often. Therefore, the Laser system is regularly used between two scans to calibrate the response of the PMTs and of the electronic readout. Unfortunately, during 2016 a water leakage was discovered and the Cesium scans were suspended. In absence of the Cesium calibration, the Laser calibration procedure acquires a leading role in the TileCal calibration.  A description of the Laser calibration system is reported in the following.

To maintain the calorimeter high performance, a continuous calibration of each cell is required.  The stability of the calorimeter response, within 1\%, is important for the jet and missing transverse momentum reconstruction. TileCal reconstructs up to 40\% of the energy of the jets with \pT > 3 \TeV. The mean fraction of the contained jet energy increases with increasing jet \pT from approximately 20\% to 40\% between 100 \GeV\xspace and 3 \TeV. To obtain a precise and stable measurement of the energy deposited in the calorimeter, it is mandatory to precisely monitor any variation of the TileCal response. If there is a response variation,  a correction is applied to restore the correct value and maintain the stability.  
%This highlights the importance of TileCal for the reconstruction of very energetic hadronic jets expected during the High-Luminosity LHC (LHC) era. A challenging goal is to understand whether the full sample of PMTs installed at the beginning of the ATLAS detector operation can be used until completion of the HL-LHC program or not. In the following the studies on the PMT response stability are shown.

\subsection{Laser calibration system}
The Laser system is used to monitor and to calibrate the PMT response. The system was upgraded during the shutdown before the beginning of the Run 2 to improve the system calibration reliability and to maintain the signal calibration at sub-percent level accuracy. \\
A sketch of the Laser II system is shown in Fig.\ref{fig:LaserII}.  The path of the laser light starts in the optics box. The light emitted by laser head is split into two beams by a beam splitter. A few percent of the reflected light is sent to three photodiodes, while the main beam is transmitted into a filter wheel. The filter wheel houses different optical density filters which are used to vary the intensity of the transmitted light.  After the filter wheel another beam splitter is placed. A fraction of the light is sent to other three photodiodes, while the remaining fraction enters into a light mixer in order to mix light and to expand the beam. The light at the light mixer exit is then collected by a bundle of fibres. Four of these fibres are connected to four photodiodes, while the other fibres, 100 m long, transmit the light to 384 light mixers located in each TileCal module. The light from each light mixer is then sent to all the PMTs reading out a TileCal module.\\ 
In the Laser calibration system the photodiodes are used to probe the laser beam stability at different steps of the light transmissions. The stability of the photodiodes is monitored with a LED whose stability is controlled by a reference photodiode. The reference photodiode is equiped with a radioactive source that allows to check independently its own stability.
\begin{figure}[!htb]
 	\centering
 	\includegraphics[width=0.6\textwidth]{figures/chapter_3/figures_LaserII_system.jpg}
 	\caption{Sketch of the Laser II system.}
 	\label{fig:LaserII}
 \end{figure}

The laser pulses have a time profile similar to those of light signals generated by particle crossing in the calorimeter cells. However, the optical path is different in the two cases and the light transmission stability of the optical path for the laser beam has to be controlled. A specific algorithm measures the drifts due to the transmission system only. A detailed description of the algorithm can be found in Ref. \cite{DiGregorio}. The algorithm is used to evaluate the Laser calibration constant $C_{\text{Laser}}$ which is applied to correct for non-uniformities in the response of the PMTs of TileCal.  In the following the PMT response variation measured during the Run 2 and the expected response variation during the HL-LHC era are presented.

\section{PMT response stability}
TileCal is readout by about 10,000 PMTs. The PMTs selected for TileCal are a special version of the Hamamatsu model R5900. The TileCal PMT version is R7877 characterized by response linearity in a huge interval of incident light intensity and fast rise time. The Laser system is used to study the PMT response evolution. 
%The PMT stability is continuously monitored using different systems: the Cesium calibration system,  the Laser calibration system and the minimum bias event. The comparison among the different data set allows different sources of signal drift that can be disentangled. This appendix is focused on the PMT response stability studied using the laser system.
Fig. \ref{fig:PMT_response_time} shows the average PMT response variation of the most exposed TileCal cells (A-cells and E-cells) during the Run 2. The average is evaluated considering all the PMTs reading out the same cell type. The response is normalised to the calibration run taken on July 17$^{\text{th}}$ 2015, at the beginning of Run 2. The PMT response evolution is characterized by down-drifts and up-drifts. The down-drifts coincide with the \textit{p-p} collision periods while the response recovery occurs during the technical stops of the LHC machine.
%\begin{figure}[!htb]
% 	\centering
% 	\includegraphics[width=0.8\textwidth]{figures/chapter_3/Cell_evolution_2015-18_Preliminary.pdf}
% 	\caption{Average PMT response variation of the most exposed TileCal cells as a function of time during the Run 2 \cite{TileCalPubPlots}.}
% 	\label{fig:PMT_response_time}
% \end{figure}

Moreover, the average PMT response can be studied as a function of the integrated anode charge, as shown in Fig. \ref{fig:PMT_response_integrated_charge}.  The response is normalised to the calibration run taken before the beginning of Run 2 (July 17$^{\text{th}}$ 2015).  The points corresponds to the average response of the PMTs reading the most exposed cells.
The behaviour of the PMT response changes when large currents are integrated.  The average PMT response is fitted with a double exponential function $R(Q)$, defined as:
\begin{equation}
R(Q) = p_0 \cdot \exp(p_1 \cdot Q) + p_2 \cdot \exp(p_3 \cdot Q)
\label{eq:double_exp}
\end{equation}
where $Q$ is the integrated anode charge. The double exponential function $R(Q)$ describes fairly well both the initial part of the response evolution and the high value region of the integrated charge. The double exponential function can be used to make projections about the expected PMT response loss in the HL-LHC. The expected integrated luminosity at the end of the HL-LHC is 4000 fb$^{-1}$, corresponding to 600 C of integrated anode charge for the PMTs looking to the light output of the most exposed cells.  The expected response loss of the PMTs reading the most exposed cells is more than 25\%. These PMTs represent 8\% of total 10,000 TileCal PMTs and they will be replaced by a newer version but with same geometry. The new PMT version is model R11187 which have been tested in the Pisa-INFN laboratory. %The drifts in the PMT response have been studied accurately in laboratory with laser and diode sources
\begin{figure}[!htbp]
  \centering
    \subfloat[]{
       \includegraphics[width=0.5\textwidth]{figures/chapter_3/Cell_evolution_2015-18_Preliminary.pdf}
       \label{fig:PMT_response_time}
    }
    \subfloat[]{
       \includegraphics[width=0.53\textwidth]{figures/chapter_3/PMT_response_VS_charge_detector_Preliminary.pdf}
       \label{fig:PMT_response_integrated_charge}
    }
  \caption{Average PMT response variation of the most exposed TileCal cells as a function of time (a) and of the integrated anode charge (b) during the Run 2 \cite{TileCalPubPlots}.}
\end{figure}

\FloatBarrier 
\subsection{PMT ageing}
A local test bench to evaluate the effect of the charge integration of the PMTs is operating at Pisa-INFN laboratory. The optical system used in the laboratory is similar to the one used in TileCal for the Laser calibration. In the laboratory, the anode charge integration is done with a pulsing green LED, while the PMT response is measured with the laser.  The current (R7877) and the new (R11187) PMT model were tested in the laboratory. The tested R7877 PMTs were dismounted from the TileCal detector in February 2017 and were reading different cell types having integrated 1 C to 5 C since the beginnig of the LHC operating period. Figure \ref{fig:PMT_response_charge_lab} shows the average PMT response as a function of the integrated anode charge for the two PMT models. The blue points represent the average response of 7 PMTs model R7877, while the red triangular points represent the average response of 4 PMTs model R11187. The PMT response is normalised to the first day of observation. The average PMT response of the two PMT groups is fitted with the double exponential function (Eq. \ref{eq:double_exp}). The new PMT model has a smaller down-drift with respect to the old PMT version. \\
In conclusion, calibration data and test bench data provide consistent results showing that the PMTs are affected by a degradation of their response. The response degradation depends on the amount of integrated anode charge.  Moreover the calibration data provide useful information to predict the PMT behaviours during the HL-LHC period. To prevent large response losses of PMTs reading the most exposed cells, 8\% of the total TileCal PMTs will be replaced with a new generation PMTs with improved performance.
\begin{figure}[!htb]
 	\centering
 	\includegraphics[width=0.5\textwidth]{figures/chapter_3/Average_PMT_deviation_VS_integrated_charge_Preliminary.pdf}
 	\caption{Average PMT relative response of the old (blue) and new (red) PMT models at the test bench \cite{TileCalPubPlots}.}
 	\label{fig:PMT_response_charge_lab}
 \end{figure}


%The stability of the PMT response in the high irradiation environment of the HL-LHC is essential to be able to keep high performance in the measurement of jets, hadronic decays of tau leptons and missing transverse momentum. The PMT stability is continuously mon- itored using different data sets: Cesium source calibrations, minimum bias events and laser calibrations. The comparison among the data sets allows different sources of signal drift in time to be disentangled. These include losses in tile scintillation efficiency, optical fibre transmission, PMT photocathode efficiency and PMT amplification gain. Drifts in the PMT response were accurately studied in the laboratory with laser and diode sources.